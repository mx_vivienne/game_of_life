use crate::constants::*;

use std::error::Error;

use sdl2::{
    Sdl,
    VideoSubsystem,
    rect::Point,
    render::Canvas,
    video::Window
};

pub struct Renderer<'ctx> {
    height: u32,
    width: u32,
    sdl_context: &'ctx Sdl,
    video_subsystem: &'ctx VideoSubsystem,
    pub canvas: Canvas<Window>,
}

impl<'ctx> Renderer<'ctx> {
    pub fn new(
        sdl_context: &'ctx Sdl, 
        video_subsystem: &'ctx VideoSubsystem,
        title: &str,
        width: u32,
        height: u32,
        scale: f32,
    ) -> Result<Renderer<'ctx>, Box<dyn Error>>
    {
        let window = video_subsystem
            .window(
                title,
                (width as f32 * scale) as u32,
                (height as f32 * scale) as u32)
            .position_centered()
            .build()?;
        let mut canvas = window.into_canvas().build()?;
        canvas.set_scale(scale, scale)?;

        Ok(Renderer {
            height,
            width,
            sdl_context,
            video_subsystem,
            canvas,
        })
    }

    pub fn clear(&mut self) {
        self.canvas.set_draw_color(COLOR_DEAD);
        self.canvas.clear();
    }

    pub fn draw_cells(&mut self, cells: &[bool], prev_cells: &[bool]) -> Result<(), Box<dyn Error>> {
        for y in 0..self.height {
            for x in 0..self.width {
                let current = cells[(y * self.width + x) as usize];
                let previous = prev_cells[(y * self.width + x) as usize];
    
                let color = match (previous, current) {
                    (true, true) => COLOR_ALIVE,
                    (true, false) => COLOR_DYING,
                    (false, true) => COLOR_REVIVING,
                    (false, false) => COLOR_DEAD,
                };

                if previous || current {
                    self.canvas.set_draw_color(color);
                    self.canvas.draw_point(Point::new(x as i32, y as i32))?;
                }
            }
        }

        Ok(())
    }
}