use sdl2::pixels::Color;
use std::ops::Range;

/// The amount of alive neighbors an alive cell needs to survive one generation.
pub const SURVIVE: Range<u8> = 2..4;
/// The amount of alive neighbors a dead cell needs to come alive.
pub const ALIVE: Range<u8> = 3..4;

pub const COLOR_DEAD: Color = Color::RGB(0, 0, 0);
pub const COLOR_ALIVE: Color = Color::RGB(255, 255, 255);
pub const COLOR_REVIVING: Color = Color::RGB(50, 200, 50);
pub const COLOR_DYING: Color = Color::RGB(200, 70, 100);