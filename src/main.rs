extern crate sdl2;
extern crate game_of_life;

use std::error::Error;
use structopt::StructOpt;
use game_of_life::{
    run, benchmark, opt::Opt,
};

fn main() -> Result<(), Box<dyn Error>> {
    let mut opt = Opt::from_args();
    opt.init_fields();

    if opt.benchmark {
        benchmark(opt)
    } else {
        run(opt)
    }
}