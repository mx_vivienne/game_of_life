use structopt::StructOpt;
use std::time::Duration;
use crate::game::BorderMode;

#[derive(StructOpt, Clone, Copy)]
#[structopt(name = "Game of Life")]
pub struct Opt {
    /// Runs in benchmark mode.
    #[structopt(short, long)]
    pub benchmark: bool,

    /// Selects how cells interact with the borders of the simulation.
    #[structopt(short="m", long, default_value="Dead")]
    pub border_mode: BorderMode,

    /// The scale at which to display the simulation.
    #[structopt(short="s", long="scale", default_value="5.0")]
    pub render_scale: f32,

    /// The amount of rows of cells.
    #[structopt(short="y", long, default_value="50")]
    pub height: u32,

    /// The amount of columns of cells.
    #[structopt(short="x", long, default_value="50")]
    pub width: u32,

    /// The targeted frame rate.
    #[structopt(short="f", long="framerate", default_value="10")]
    pub ticks_per_second: u16,

    /// The time slept between frames.
    #[structopt(skip)]
    pub frame_delta: Duration,

    /// For how many generations to run.
    #[structopt(short, long)]
    pub generations: Option<u32>,

    /// Supress output in benchmark mode.
    #[structopt(short, long)]
    pub quiet: bool,
}

impl Opt {
    pub fn init_fields(&mut self) {
        self.frame_delta = Duration::from_nanos(1_000_000_000u64 / self.ticks_per_second as u64);
    }
}