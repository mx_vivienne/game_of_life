use std::{
    collections::{
        hash_set::HashSet,
        hash_map::HashMap,
    },
    iter::Iterator,
    fmt::{self, Display},
    mem::swap,
};

use rand::{thread_rng, Rng};
use itertools::iproduct;

use crate::constants::*;
use super::border_mode::BorderMode;

#[derive(Hash, Eq, PartialEq, Clone, Copy)]
pub struct Position {
    x: usize,
    y: usize,
}

pub struct GameHashSet {
    width: usize,
    height: usize,
    border_mode: BorderMode,
    cells: HashSet<Position>,
    prev_cells: HashSet<Position>,
    counts: HashMap<Position, u8>,
}

impl GameHashSet {
    /// Create a new game of life simulation.
    ///
    /// # Panics
    ///
    /// If either dimension has a size of 1 or 0.
    pub fn new(width: usize, height: usize, border_mode: BorderMode) -> Self {
        assert!(width > 1 && height > 1, "invalid size");

        Self {
            width,
            height,
            border_mode,
            cells: HashSet::with_capacity(width * height),
            prev_cells: HashSet::with_capacity(width * height),
            counts: HashMap::with_capacity(width * height),
        }
    }

    /// Set the current state of the simulation.
    ///
    /// # Panics
    ///
    /// If the length of cells does not match the amount of cells in the simulation (ie. height * width).
    pub fn set_state(&mut self, cells: &[bool]) {
        assert_eq!(self.width * self.height, cells.len(), "state has invalid size");

        let width = self.width;
        self.cells.clear();
        self.cells.extend(cells.iter().enumerate().filter_map(
            |(i, &b)| b.then(|| Position {x: i % width, y: i / width})
        ));
    }

    /// Get a vector representing the current state of the simulation.
    pub fn get_state(&self) -> Vec<bool> {
        iproduct!(0..self.height, 0..self.width)
            .map(|(y, x)| self.cells.contains(&Position{x, y}))
            .collect()
    }

    /// Get an immutable reference to the state during the previous tick of the simulation.
    pub fn get_previous_state(&self) -> &[bool] {
        todo!()
    }

    /// Get the number of rows of cells simulated.
    pub fn get_height(&self) -> usize {
        self.height
    }

    /// Get the number of columns of cells simulated.
    pub fn get_width(&self) -> usize {
        self.width
    }

    /// Get the current state of an individual cell.
    pub fn get_cell(&self, pos: &Position) -> bool {
        self.cells.contains(pos)
    }

    /// Fill the simulation with cells randomly.
    pub fn randomize(&mut self) {
        let mut state = vec![false; self.width * self.height];
        thread_rng().fill(state.as_mut_slice());
        self.set_state(state.as_slice());
    }

    /// Calculate one tick of the simulation,
    /// and update the current and previous stored states accordingly.
    pub fn update(&mut self) {
        // Count each cell's alive neighbors by iterating over all cells,
        // and for each alive one increment its neighbor's counts by one.
        //
        // Note that multiple separate loops are used, so that all "inner" cells
        // not adjacent to the border of the simulation can use the unchecked
        // function to skip unnecessary bounds checking.

        self.counts.clear();
        for &cell in self.cells.iter() {
            if cell.x == 0 || cell.x + 1 == self.width
            || cell.y == 0 || cell.y + 1 == self.height {
                increase_neighbor_count(&mut self.counts, cell, self.width, self.height);
            } else {
                increase_neighbor_count_unchecked(&mut self.counts, cell);
            }
        }
        
        /*
        // Now increase the counts of all cells
        // adjacent to the simulation boundaries according to the BorderMode used.
        self.border_mode.correct_borders(
            self.counts.as_mut_slice(), 
            self.cells.as_slice(),
            self.width, 
            self.height);
        */

        // Move current state to prev_cells before overwriting it
        swap(&mut self.prev_cells, &mut self.cells);

        let prev_cells = &self.prev_cells;
        let counts = &self.counts;

        self.cells.clear();
        self.cells.extend(iproduct!(0..self.width, 0..self.height)
            .filter_map(|(x, y)| {
                let pos = Position{ x, y };
                let cell_state = prev_cells.contains(&pos);
                let cell_count = counts.get(&pos).unwrap_or(&0);

                ((cell_state && SURVIVE.contains(cell_count)) || 
                (!cell_state && ALIVE.contains(cell_count))).then(|| pos)
            })
        );
    }
}

/// Assume the indicated cell is alive and increase the counts of all neighboring cells.
/// This function is the checked version, which means it does bounds checking to ensure
/// it only increments actually existing neighbors.
fn increase_neighbor_count(
    counts: &mut HashMap<Position, u8>,
    pos: Position,
    width: usize,
    height: usize
) {
    let mut increment = |x, y| {
        *counts.entry(Position{x, y}).or_default() += 1;
    };

    // cells in the row above
    if pos.y > 0 {
        if pos.x > 0            {increment(pos.x - 1, pos.y - 1)}
                                 increment(pos.x,     pos.y - 1);
        if pos.x + 1 < width    {increment(pos.x + 1, pos.y - 1)}
    }

    // cell to the left
    if pos.x > 0            {increment(pos.x - 1, pos.y)}
    // cell to the right
    if pos.x + 1 < width    {increment(pos.x + 1, pos.y)}

    // cells in the row below
    if pos.y + 1 < height {
        if pos.x > 0            {increment(pos.x - 1, pos.y + 1)}
                                 increment(pos.x,     pos.y + 1);
        if pos.x + 1 < width    {increment(pos.x + 1, pos.y + 1)}
    }
}

fn increase_neighbor_count_unchecked(
    counts: &mut HashMap<Position, u8>,
    pos: Position,
) {
    let mut increment = |x, y| {
        *counts.entry(Position{x, y}).or_default() += 1;
    };

    // neighbors in the row above
    increment(pos.x - 1, pos.y - 1);
    increment(pos.x    , pos.y - 1);
    increment(pos.x + 1, pos.y - 1);
    
    // neighbors left and right
    increment(pos.x - 1, pos.y);
    increment(pos.x + 1, pos.y);

    // neighbors in the row below
    increment(pos.x - 1, pos.y + 1);
    increment(pos.x    , pos.y + 1);
    increment(pos.x + 1, pos.y + 1);
}

impl Display for GameHashSet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut grid = String::with_capacity((self.width + 1) * self.height);

        for row in 0..self.height {
            for column in 0..self.width {
                grid.push(match self.cells.contains(&Position {x: column, y: row}) {
                    true => '■',
                    false => '□',
                });
            }
            grid.push('\n');
        }

        write!(f, "{}", grid)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
        
    #[test]
    fn glider_3_generations_dead_borders() {
        let mut game = GameHashSet::new(5, 5, BorderMode::Dead);
        game.set_state(&[
            false, true,  false, false, false,
            false, false, true,  false, false,
            true,  true,  true,  false, false,
            false, false, false, false, false,
            false, false, false, false, false,
        ]);

        for _i in 0..3 {
            println!("{}\n{}", _i, game);
            game.update();
        }
        println!("{}\n{}", 3, game);

        assert_eq!(game.get_state().as_slice(), &[
            false, false, false, false, false,
            false, true,  false, false, false,
            false, false, true,  true,  false,
            false, true,  true,  false, false,
            false, false, false, false, false,
        ]);
    }

    #[test]
    fn empty_3_generations_alive_borders() {
        let mut game = GameHashSet::new(5, 5, BorderMode::Alive);
        game.set_state(&[false; 25]);

        for _i in 0..3 {
            println!("{}\n{}", _i, game);
            game.update()
        }
        println!("{}\n{}", 3, game);

        assert_eq!(game.get_state().as_slice(), &[
            false, false, false, false, false,
            false, false, true,  false, false,
            false, true,  false, true,  false,
            false, false, true,  false, false,
            false, false, false, false, false,
        ]);
    }

    #[test]
    fn glider_20_generations_wrap_borders() {
        let glider = [
            false, false, true,  false, false,
            true,  false, true,  false, false,
            false, true,  true,  false, false,
            false, false, false, false, false,
            false, false, false, false, false,
        ];

        let mut game = GameHashSet::new(5, 5, BorderMode::Wrap);
        game.set_state(&glider);

        for _i in 0..20 {
            println!("{}\n{}", _i, game);
            game.update();
        }
        println!("{}\n{}", 20, game);

        assert_eq!(&glider, game.get_state().as_slice());
    }
}