use std::str::FromStr;
use std::fmt::{self, Display};
use rand::Rng;
use super::game::index;

#[derive(Debug)]
pub struct InvalidBorderMode {
    input: String,
}

impl Display for InvalidBorderMode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unrecognized BorderMode \"{}\"", self.input)
    }
}

/// Options for how to treat the borders of the simulation.
#[derive(Copy, Clone)]
pub enum BorderMode {
    /// Treat the border as always dead cells.
    Dead,

    /// Treat the border as always alive cells.
    Alive,

    /// Lookup the cells on the opposite side of the simulation.
    Wrap,

    /// Like Wrap, but lookups across the sides are flipped along the y-axis.
    FlipY,

    /// Like Wrap, but lookups across the top or bottom are flipped along the x-axis.
    FlipX,

    /// Like FlipY and FlipX combined
    FlipBoth,

    /// For each tick, the border gets new randomized cells.
    Random,
}

impl Display for BorderMode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            BorderMode::Dead => "dead",
            BorderMode::Alive => "alive",
            BorderMode::Wrap => "wrap",
            BorderMode::Random => "random",
            BorderMode::FlipY => "flip_y",
            BorderMode::FlipX => "flip_x",
            BorderMode::FlipBoth => "flip_both",
        })
    }
}

impl FromStr for BorderMode {
    type Err = InvalidBorderMode;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "dead" => Ok(BorderMode::Dead),
            "alive" => Ok(BorderMode::Alive),
            "wrap" => Ok(BorderMode::Wrap),
            "random" => Ok(BorderMode::Random),
            "flipy" | "flip_y" => Ok(BorderMode::FlipY),
            "flipx" | "flip_x" => Ok(BorderMode::FlipX),
            //"flipboth" | "flip_both" => Ok(BorderMode::FlipBoth),
            _ => Err(InvalidBorderMode {input: s.to_owned()})
        }
    }
}

impl BorderMode {
    pub fn correct_borders(&self, counts: &mut [u8], cells: &[bool], width: usize, height: usize) {
        match *self {
            BorderMode::Dead => {},
            BorderMode::Alive => {correct_borders_alive(counts, width, height)},
            BorderMode::Random => {correct_borders_random(counts, width, height)},
            BorderMode::Wrap => {correct_borders_wrap(counts, cells, width, height,
                |x| {x},
                |y| {y}
            )},
            BorderMode::FlipY => {correct_borders_wrap(counts, cells, width, height,
                |x| {x},
                |y| {height - 1 - y}
            )},
            BorderMode::FlipX => {correct_borders_wrap(counts, cells, width, height,
                |x| {width - 1 - x},
                |y| {y}
            )},
            BorderMode::FlipBoth => {correct_borders_wrap(counts, cells, width, height,
                |x| {width - 1 - x},
                |y| {height - 1 - y}
            )},
        }
    }

}

fn correct_borders_alive(counts: &mut [u8], width: usize, height: usize) {
    // correct top/bottom rows
    for row in [0, height - 1] {
        for column in 1..width - 1 {
            counts[index(column, row, width)] += 3;
        }
    }

    // correct left/right columns
    for row in 1..height - 1 {
        for column in [0, width - 1] {
            counts[index(column,row, width)] += 3;
        }
    }

    // correct corners
    for row in [0, height - 1] {
        for column in [0, width - 1] {
            counts[index(column, row, width)] += 5;
        }
    }
}

fn correct_borders_wrap<X, Y>(
    counts: &mut [u8],
    cells: &[bool],
    width: usize,
    height: usize,
    x_tf: X,
    y_tf: Y
) where
    X: Copy + FnOnce(usize) -> usize, 
    Y: Copy + FnOnce(usize) -> usize
{
    let last_row = height - 1;
    let last_column = width - 1;

    // correct top/bottom rows
    for row in [0, last_row] {
        let y = y_tf(row);
        let opposite = last_row - y;

        for column in 1..last_column {
            let x = x_tf(column);
            counts[index(column, row, width)] +=
                cells[index(x - 1, opposite, width)] as u8 +
                cells[index(x    , opposite, width)] as u8 +
                cells[index(x + 1, opposite, width)] as u8;
        }
    }

    // correct left/right columns
    for column in [0, last_column] {
        let x = x_tf(column);
        let opposite = last_column - x;

        for row in 1..last_row {
            let y = y_tf(row);
            counts[index(column, row, width)] +=
                cells[index(opposite, y - 1, width)] as u8 +
                cells[index(opposite, y    , width)] as u8 +
                cells[index(opposite, y + 1, width)] as u8;
        }
    }

    // correct top-left corner
    counts[index(0, 0, width)] +=
        cells[index(x_tf(1),           y_tf(last_row), width)] as u8 +
        cells[index(x_tf(0),           y_tf(last_row), width)] as u8 +
        cells[index(x_tf(last_column), y_tf(last_row), width)] as u8 +
        cells[index(x_tf(last_column), y_tf(0),        width)] as u8 +
        cells[index(x_tf(last_column), y_tf(1),        width)] as u8;

    // correct top-right corner
    counts[index(last_column, 0, width)] +=
        cells[index(x_tf(last_column - 1), y_tf(last_row), width)] as u8 +
        cells[index(x_tf(last_column),     y_tf(last_row), width)] as u8 +
        cells[index(x_tf(0),               y_tf(last_row), width)] as u8 +
        cells[index(x_tf(0),               y_tf(0),        width)] as u8 +
        cells[index(x_tf(0),               y_tf(1),        width)] as u8;

    // correct bottom-left corner
    counts[index(0, last_row, width)] +=
        cells[index(x_tf(1),           y_tf(0),            width)] as u8 +
        cells[index(x_tf(0),           y_tf(0),            width)] as u8 +
        cells[index(x_tf(last_column), y_tf(0),            width)] as u8 +
        cells[index(x_tf(last_column), y_tf(last_row),     width)] as u8 +
        cells[index(x_tf(last_column), y_tf(last_row - 1), width)] as u8;

    // correct bottom-right corner
    counts[index(last_column, last_row, width)] +=
        cells[index(x_tf(last_column - 1), y_tf(0),            width)] as u8 +
        cells[index(x_tf(last_column),     y_tf(0),            width)] as u8 +
        cells[index(x_tf(0),               y_tf(0),            width)] as u8 +
        cells[index(x_tf(0),               y_tf(last_row),     width)] as u8 +
        cells[index(x_tf(0),               y_tf(last_row - 1), width)] as u8;
}

fn correct_borders_random(counts: &mut [u8], width: usize, height: usize) {
    let mut rows = vec![false; 2 * width];
    let mut columns = vec![false; 2 * height];
    let mut corners = vec![false; 4];

    let mut rng = rand::thread_rng();
    rng.fill(rows.as_mut_slice());
    rng.fill(columns.as_mut_slice());
    rng.fill(corners.as_mut_slice());

    let last_row = height - 1;
    let last_column = width - 1;

    // correct top/bottom rows
    for (section, &row) in [0, last_row].iter().enumerate() {
        for column in 1..last_column {
            counts[index(column, row, width)] +=
                rows[section * width + column - 1] as u8 +
                rows[section * width + column    ] as u8 +
                rows[section * width + column + 1] as u8;
        }
    }

    // correct left/right columns
    for (section, &column) in [0, last_column].iter().enumerate() {
        for row in 1..last_row {
            counts[index(column, row, width)] +=
                columns[section * height + row - 1] as u8 +
                columns[section * height + row    ] as u8 +
                columns[section * height + row + 1] as u8;
        }
    }

    // correct top-left corner
    counts[index(0, 0, width)] +=
        rows[1] as u8 +
        rows[0] as u8 +
        corners[0] as u8 +
        columns[0] as u8 +
        columns[1] as u8;

    // correct top-right corner
    counts[index(last_column, 0, width)] +=
        rows[last_column - 1] as u8 +
        rows[last_column] as u8 +
        corners[1] as u8 +
        columns[height] as u8 +
        columns[height + 1] as u8;

    // correct bottom-left corner
    counts[index(0, last_row, width)] +=
        rows[width] as u8 +
        rows[width + 1] as u8 +
        corners[2] as u8 +
        columns[last_row] as u8 +
        columns[last_row - 1] as u8;

    // correct bottom-right corner
    counts[index(last_column, last_row, width)] +=
        rows[2 * width - 2] as u8 +
        rows[2 * width - 1] as u8 +
        corners[3] as u8 +
        columns[2 * height - 1] as u8 +
        columns[2 * height - 2] as u8;
}