use crate::constants::*;
use rand::{thread_rng, Rng};
use std::fmt::{self, Display};
use std::mem::{take, swap};
use core::iter::Iterator;
use super::border_mode::BorderMode;

pub struct Game {
    width: usize,
    height: usize,
    border_mode: BorderMode,
    cells: Vec<bool>,
    prev_cells: Vec<bool>,
    counts: Vec<u8>,
}

impl Game {
    /// Create a new game of life simulation.
    ///
    /// # Panics
    ///
    /// If either dimension has a size of 1 or 0.
    pub fn new(width: usize, height: usize, border_mode: BorderMode) -> Self {
        assert!(width > 1 && height > 1, "invalid size");

        Self {
            width,
            height,
            border_mode,
            cells: vec![false; width * height],
            prev_cells: vec![false; width * height],
            counts: vec![0; width * height],
        }
    }

    /// Set the current state of the simulation.
    ///
    /// # Panics
    ///
    /// If the length of cells does not match the amount of cells in the simulation (ie. height * width).
    pub fn set_state(&mut self, cells: &[bool]) {
        assert_eq!(self.cells.len(), cells.len(), "state has invalid size");

        self.cells.clear();
        self.cells.extend_from_slice(cells);
    }

    /// Get an immutable reference to the current state of the simulation.
    pub fn get_state(&self) -> &[bool] {
        self.cells.as_slice()
    }

    /// Get an immutable reference to the state during the previous tick of the simulation.
    pub fn get_previous_state(&self) -> &[bool] {
        self.prev_cells.as_slice()
    }

    /// Get the number of rows of cells simulated.
    pub fn get_height(&self) -> usize {
        self.height
    }

    /// Get the number of columns of cells simulated.
    pub fn get_width(&self) -> usize {
        self.width
    }

    /// Get the current state of an individual cell.
    pub fn get_cell(&self, x: usize, y: usize) -> bool {
        self.cells[index(x, y, self.width)]
    }

    /// Fill the simulation with cells randomly.
    pub fn randomize(&mut self) {
        thread_rng().fill(self.cells.as_mut_slice());
    }

    /// Calculate one tick of the simulation,
    /// and update the current and previous stored states accordingly.
    pub fn update(&mut self) {
        let width = self.width;
        let height = self.height;

        // Count each cell's alive neighbors by iterating over all cells,
        // and for each alive one increment its neighbor's counts by one.
        //
        // Note that multiple separate loops are used, so that all "inner" cells
        // not adjacent to the border of the simulation can use the unchecked
        // function to skip unnecessary bounds checking.

        // top row
        (0..self.width)
            .for_each(|index| if self.cells[index]
                {self.increase_neighbor_count(index)}
        );

        // inner rows
        for row_start in (1..self.height - 1).map(|r| r * width) {
            // left column
            if self.cells[row_start] {
                self.increase_neighbor_count(row_start);
            }

            // inner columns
            (1..self.width - 1).map(|i| row_start + i)
                .for_each(|index| if self.cells[index]
                    {self.increase_neighbor_count_unchecked(index)}
            );

            // right column
            let right_cell = row_start + self.width - 1;
            if self.cells[right_cell] {
                self.increase_neighbor_count(right_cell);
            }
        }

        // bottom row
        (0..self.width).map(|i| (height - 1) * width + i)
            .for_each(|index| {
                if self.cells[index]
                    {self.increase_neighbor_count(index)}
            }
        );
        
        /*
        // Now increase the counts of all cells
        // adjacent to the simulation boundaries according to the BorderMode used.
        self.border_mode.correct_borders(
            self.counts.as_mut_slice(), 
            self.cells.as_slice(),
            self.width, 
            self.height);
        */

        // Move current state to prev_cells before overwriting it
        swap(&mut self.prev_cells, &mut self.cells);

        self.cells.clear();
        self.cells.extend(
            Iterator::zip(
                self.prev_cells.iter(),
                self.counts.iter_mut()
            ).map(|(cell, count)| {
                // Take the value and reset the count to 0
                let count_val = take(count);

                // Check if this cell is supposed to be alive or dead next tick
                (*cell && SURVIVE.contains(&count_val)) ||
                (!*cell && ALIVE.contains(&count_val))
            })
        );
    }

    /// Assume the indicated cell is alive and increase the counts of all neighboring cells.
    /// This function is the checked version, which means it does bounds checking to ensure
    /// it only increments actually existing neighbors.
    fn increase_neighbor_count(&mut self, index: usize) {
        let row = index / self.width;
        let column = index % self.width;

        // cells in the row above
        if row > 0 {
            if column > 0               {self.counts[index - self.width - 1] += 1}
                                         self.counts[index - self.width    ] += 1;
            if column + 1 < self.width  {self.counts[index - self.width + 1] += 1}
        }

        // cell to the left
        if column > 0               {self.counts[index - 1] += 1}
        // cell to the right
        if column + 1 < self.width  {self.counts[index + 1] += 1}

        // cells in the row below
        if row + 1 < self.height {
            if column > 0               {self.counts[index + self.width - 1] += 1}
                                         self.counts[index + self.width    ] += 1;
            if column + 1 < self.width  {self.counts[index + self.width + 1] += 1}
        }
    }

    fn increase_neighbor_count_unchecked(&mut self, index: usize) {
        assert!(self.counts.len() > index + self.width + 1, "index out of bounds");

        // neighbors in the row above
        self.counts[index - self.width - 1] += 1;
        self.counts[index - self.width    ] += 1;
        self.counts[index - self.width + 1] += 1;
        
        // neighbors left and right
        self.counts[index - 1] += 1;
        self.counts[index + 1] += 1;

        // neighbors in the row below
        self.counts[index + self.width - 1] += 1;
        self.counts[index + self.width    ] += 1;
        self.counts[index + self.width + 1] += 1;
    }
}

/// Calculates an index from x/y coordinates and the width of the grid.
pub fn index(x: usize, y: usize, w: usize) -> usize {
    y * w + x
}

impl Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut grid = String::with_capacity((self.width + 1) * self.height);

        for row in 0..self.height {
            for column in 0..self.width {
                grid.push(match self.cells[index(column, row, self.width)] {
                    true => '■',
                    false => '□',
                });
            }
            grid.push('\n');
        }

        write!(f, "{}", grid)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
        
    #[test]
    fn glider_3_generations_dead_borders() {
        let mut game = Game::new(5, 5, BorderMode::Dead);
        game.set_state(&[
            false, true,  false, false, false,
            false, false, true,  false, false,
            true,  true,  true,  false, false,
            false, false, false, false, false,
            false, false, false, false, false,
        ]);

        for _i in 0..3 {
            println!("{}\n{}", _i, game);
            game.update();
        }
        println!("{}\n{}", 3, game);

        assert_eq!(game.get_state(), &[
            false, false, false, false, false,
            false, true,  false, false, false,
            false, false, true,  true,  false,
            false, true,  true,  false, false,
            false, false, false, false, false,
        ]);
    }

    #[test]
    fn empty_3_generations_alive_borders() {
        let mut game = Game::new(5, 5, BorderMode::Alive);
        game.set_state(&[false; 25]);

        for _ in 0..3 {game.update()}

        assert_eq!(game.get_state(), &[
            false, false, false, false, false,
            false, false, true,  false, false,
            false, true,  false, true,  false,
            false, false, true,  false, false,
            false, false, false, false, false,
        ]);
    }

    #[test]
    fn glider_20_generations_wrap_borders() {
        let glider = [
            false, false, true,  false, false,
            true,  false, true,  false, false,
            false, true,  true,  false, false,
            false, false, false, false, false,
            false, false, false, false, false,
        ];

        let mut game = Game::new(5, 5, BorderMode::Wrap);
        game.set_state(&glider);

        for _i in 0..20 {
            //println!("{}\n{}", _i, game);
            game.update();
        }
        //println!("{}\n{}", 20, game);

        assert_eq!(&glider, game.get_state());
    }
}