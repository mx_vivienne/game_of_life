pub mod constants;
pub mod render;
pub mod game;
pub mod opt;

use std::{
    error::Error,
    iter::repeat,
    time::SystemTime,
};

use sdl2::{
    event::Event,
    keyboard::Keycode,
};

use crate::{
    opt::Opt,
    game::{Game, GameHashSet},
    render::Renderer,
};

#[inline]
pub fn run(mut opt: Opt) -> Result<(), Box<dyn Error>> {
    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;
    let mut renderer = Renderer::new(
        &sdl_context,
        &video_subsystem,
        "Game of Life",
        opt.width,
        opt.height,
        opt.render_scale,
    )?;

    let mut event_pump = sdl_context.event_pump()?;
    let mut game = Game::new(
        opt.width as usize,
        opt.height as usize,
        opt.border_mode
    );
    game.randomize();

    renderer.clear();
    renderer.canvas.present();

    'game: loop {
        renderer.clear();

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown {keycode: Some(Keycode::Escape), ..} => {
                    break 'game
                },
                _ => {},
            }
        }

        if opt.generations.unwrap_or(1) == 0 {
            break 'game
        }
        opt.generations = opt.generations.map(|g| g - 1);

        renderer.draw_cells(game.get_state(), game.get_previous_state())?;
        renderer.canvas.present();

        game.update();
        std::thread::sleep(opt.frame_delta);
    }

    Ok(())
}

#[inline]
pub fn benchmark(opt: Opt) -> Result<(), Box<dyn Error>> {
    let gens = opt.generations
        .expect("generations need to be set in benchmark mode");

    let mut game = Game::new(
        opt.width as usize,
        opt.height as usize,
        opt.border_mode,
    );
    game.randomize();

    let start = SystemTime::now();
    for _ in 0..gens {
        game.update();
    }

    let duration = SystemTime::now().duration_since(start)?;

    if !opt.quiet {
        println!(concat!(
            "       time: {}.{}s\n",
            "generations: {}\n",
            "       size: {}×{}\n",
            "border mode: {}\n"),
            duration.as_secs(), duration.as_millis() % 1000,
            gens,
            opt.width, opt.height,
            opt.border_mode
        );
    }

    Ok(())
}

#[inline]
pub fn benchmark_hashed(opt: Opt) -> Result<(), Box<dyn Error>> {
    let gens = opt.generations
        .expect("generations need to be set in benchmark mode");

    let mut game = GameHashSet::new(
        opt.width as usize,
        opt.height as usize,
        opt.border_mode,
    );
    game.randomize();

    let start = SystemTime::now();
    for _ in 0..gens {
        game.update();
    }

    let duration = SystemTime::now().duration_since(start)?;

    if !opt.quiet {
        println!(concat!(
            "       time: {}.{}s\n",
            "generations: {}\n",
            "       size: {}×{}\n",
            "border mode: {}\n"),
            duration.as_secs(), duration.as_millis() % 1000,
            gens,
            opt.width, opt.height,
            opt.border_mode
        );
    }

    Ok(())
}