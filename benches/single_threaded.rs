use criterion::{BenchmarkId, black_box, criterion_group, criterion_main, Criterion};
use structopt::StructOpt;
use game_of_life::{
	game::BorderMode,
	opt::Opt,
	benchmark,
	benchmark_hashed,
};

pub fn benchmark_single_threaded(c: &mut Criterion) {
	let mut group = c.benchmark_group("single-threaded");
	let mut opt = Opt::from_iter(["game_of_life"]);
	opt.border_mode = BorderMode::Dead;
	opt.generations = Some(100);
	opt.quiet = true;

	for (x, y) in [
		(100, 50),
		(150, 150),
		(200, 600),
		/*
		(400, 600),
		(600, 900),
		(1000, 1000),
		*/
	].iter() {
		let mut arg = opt;
		arg.width = *x;
		arg.height = *y;

		group.bench_with_input(
			BenchmarkId::new("main", x * y),
			&arg,
			|b, a| {b.iter(|| {
				benchmark(black_box(*a)).unwrap();
			})},
		);

		group.bench_with_input(
			BenchmarkId::new("hashset", x * y),
			&arg,
			|b, a| {b.iter(|| {
				benchmark_hashed(black_box(*a)).unwrap();
			})},
		);
	}
}

criterion_group!(
	name = benches;
	config = Criterion::default();
	targets =
		benchmark_single_threaded,
);
criterion_main!(benches);